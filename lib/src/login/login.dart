import 'package:flutter/material.dart';
import './loginController.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/fondo.jpg'),
            fit: BoxFit.cover,
          ),
        ),        
        child: Column(children: <Widget>[
      SizedBox(height: 150.0),
      Container(
        child: Image(image: AssetImage('assets/images/logo.png')),
        padding: EdgeInsets.symmetric(horizontal: 120.0),
        ),
        SizedBox(height: 50.0),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 15.0),
          child: new Card(
        color: Colors.white70,
        child: new Container(
          padding: EdgeInsets.all(10.0),
          child: new Column(
            children: <Widget>[
              TextField(decoration: InputDecoration(labelText: "Email"),),
              TextField(decoration: InputDecoration(labelText: "Password"),obscureText: true,),
              SizedBox(height: 50.0),
              RaisedButton(
                onPressed: (){
                  new LoginController().test(context);
                },
                child: Text(
                  'Login',
                  style: TextStyle(fontSize: 20)
                ),
              ),              
            ],
          ),
        ),
      )
        )
    ])
        )
    );
  }
}

